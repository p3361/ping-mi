from telebot import types
from emoji import emojize

from app.lib.miro import IdeaFields

# MESSAGES
INFO_TEXT = "Hi, I'm PingMi bot! {} \n\n" \
            "Write your ideas in Telegram Bot, save in Miro board. \n\n\n" \
            "I’m here to help you and your team handle with ideas backlog quickly. " \
            "Just send me your idea and I will send ir and organize it in your team Miro board😎. \n\n" \
            "Want to discuss your idea with team? Organize brainstorm session 🐤! \n\n" \
            "Every morning before or after your call with the team, the bot will remind you about idea backlog.".format(
                emojize(":robot_face:", use_aliases=True),
            )

# BUTTONS
INFO_BTN = "{} Info".format(emojize(":information:", use_aliases=True))
IDEAS_BTN = "{} Ideas".format(emojize(":light_bulb:", use_aliases=True))
BACK_BTN = "{}".format(emojize(":back:", use_aliases=True))

DESIGN_BTN = "{} {}".format(emojize(":pencil:", use_aliases=True), IdeaFields.design.value)
DEVELOPMENT_BTN = "{} {}".format(emojize(":laptop:", use_aliases=True), IdeaFields.development.value)
MARKETING_BTN = "{} {}".format(emojize(":chart_increasing:", use_aliases=True), IdeaFields.marketing.value)
SALES_BTN = "{} {}".format(emojize(":dollar:", use_aliases=True), IdeaFields.sales.value)

LIKE_GRADE_BUTTON = "{}".format(emojize(":thumbs_up:", use_aliases=True))
UNLIKE_GRADE_BUTTON = "{}".format(emojize(":thumbs_down:", use_aliases=True))

SCORE_1_BTN = "1😢"
SCORE_2_BTN = "2🤔"
SCORE_3_BTN = "3😜"
SCORE_4_BTN = "4👍"
SCORE_5_BTN = "5🔥"


# KEYBOARDS
def default_keyboard():
    """Default keyboard."""
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    info_btn = types.KeyboardButton(INFO_BTN)
    ideas_btn = types.KeyboardButton(IDEAS_BTN)
    keyboard.row(ideas_btn, info_btn)
    return keyboard


def fields_keyboard():
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    design_btn = types.KeyboardButton(DESIGN_BTN)
    development_btn = types.KeyboardButton(DEVELOPMENT_BTN)
    marketing_btn = types.KeyboardButton(MARKETING_BTN)
    sales_btn = types.KeyboardButton(SALES_BTN)

    back_btn = types.KeyboardButton(BACK_BTN)
    keyboard.row(design_btn, development_btn)
    keyboard.row(marketing_btn, sales_btn)
    keyboard.row(back_btn)
    return keyboard


def idea_score_keyboard():
    keyboard = types.InlineKeyboardMarkup(row_width=5)
    score_1_btn = types.InlineKeyboardButton(SCORE_1_BTN, callback_data="score1")
    score_2_btn = types.InlineKeyboardButton(SCORE_2_BTN, callback_data="score2")
    score_3_btn = types.InlineKeyboardButton(SCORE_3_BTN, callback_data="score3")
    score_4_btn = types.InlineKeyboardButton(SCORE_4_BTN, callback_data="score4")
    score_5_btn = types.InlineKeyboardButton(SCORE_5_BTN, callback_data="score5")
    keyboard.row(score_1_btn, score_2_btn, score_3_btn, score_4_btn, score_5_btn)
    return keyboard
