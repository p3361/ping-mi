import telebot

from app.config import TELEGRAM_BOT_TOKEN
from app.lib.telegram.keyboards import idea_score_keyboard
from app.db import IdeaRateMessage, UserIdea, TgUser


bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)


def send_ideas_for_rate(idea: UserIdea, chat_id: int):
    keyboard = idea_score_keyboard()
    text = "Hey! \n\n" \
           "How you rate this idea?\n" \
            "\"{}\"".format(idea.idea)
    res = bot.send_message(chat_id, text, parse_mode="Markdown", reply_markup=keyboard)
    if res:
        user = TgUser.get_user(res.chat.id)
        IdeaRateMessage.add_rate_message(res.message_id, user.user_id, idea.id)
