import requests
from enum import Enum

from app.config import MIRO_URL, MIRO_TOKEN


class IdeaFields(str, Enum):
    design = "Design"
    development = "Development"
    marketing = "Marketing"
    sales = "Sales"


Y_POSITION = -6000.0

field_positions = {
    IdeaFields.development.value: -12182.0,
    IdeaFields.design.value: -5096.0,
    IdeaFields.marketing.value: 2350.0,
    IdeaFields.sales.value: 9618.0,
}

LEADERBOARD_STICKERS = (3074457368055447213, 3074457368055447375, 3074457368055447452)

field_color = {
    IdeaFields.design.value: "#fff9b1",
    IdeaFields.development.value: "#d0e17a",
    IdeaFields.marketing.value: "#23bfe7",
    IdeaFields.sales.value: "#b384bb",
}

SCALE = 11.0
WIDTH = 350.0
HEIGHT = 228.0


def _get_user_name(idea):
    user_name = "Nemo"
    if idea.user.tg:
        tg_user = idea.user.tg[0]
        user_name = tg_user.first_name or tg_user.username
    elif idea.user.slack:
        user_name = "Slack"
    return user_name


def add_sticker(idea_id: int):
    from app.db import MiroSticker, TgUser, UserIdea

    idea = UserIdea.get_idea(idea_id)
    if not idea:
        return
    field = idea.field
    user_name = _get_user_name(idea)

    text = "<p>\"{}\"</p>" \
           "<p>({})</p>" \
           "<p>score: {}</p>".format(idea.idea, user_name, 0)
    num_of_stickers = MiroSticker.num_of_stickers(field) + 1
    payload = {
        "type": "sticker",
        "text": text,
        "x": field_positions[field],
        "y": Y_POSITION + 1.1*HEIGHT*SCALE*num_of_stickers,
        "width": WIDTH,
        "height": HEIGHT,
        "scale": SCALE,
        "style": {"backgroundColor": field_color[field]}
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer %s" % MIRO_TOKEN
    }
    response = requests.request("POST", MIRO_URL + "widgets/", json=payload, headers=headers)
    if response.status_code >= 300:
        return
    data = response.json()
    MiroSticker.add_sticker(idea_id, data)
    return data


def update_sticker_score(idea_id):
    from app.db import UserIdea, IdeaScore

    idea = UserIdea.get_idea(idea_id)
    if not idea:
        return
    if not idea.sticker:
        return
    score = round(IdeaScore.get_idea_score(idea_id), 2)
    sticker = idea.sticker[0]
    user_name = _get_user_name(idea)
    sticker_data = sticker.data
    sticker_id = sticker_data["id"]
    text = "<p>\"{}\"</p>" \
           "<p>({})</p>" \
           "<p>score: {}</p>".format(idea.idea, user_name, score)

    payload = {
        "text": text
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer %s" % MIRO_TOKEN
    }
    requests.request("PATCH", MIRO_URL + "widgets/%s" % sticker_id, json=payload, headers=headers)


def update_sort_stickers(idea_id):
    from app.db import UserIdea, MiroSticker, IdeaScore

    idea = UserIdea.get_idea(idea_id)
    field = idea.field
    stickers = MiroSticker.get_stickers(field)
    coordinates = [(s.data["x"], s.data["y"]) for s in stickers]
    ideas = [(s.idea, s.idea.score) for s in stickers]
    sorted_ideas = [i[0] for i in sorted(ideas, key=lambda x: x[1], reverse=True)]
    for i, c in zip(sorted_ideas, coordinates):
        sticker = i.sticker[0]
        sticker_id = sticker.data["id"]
        payload = {
            "x": c[0],
            "y": c[1]
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer %s" % MIRO_TOKEN
        }
        response = requests.request("PATCH", MIRO_URL + "widgets/%s" % sticker_id, json=payload, headers=headers)
        data = response.json()
        MiroSticker.update_sticker_data(sticker.id, data)


def update_winners_board():
    from app.db import UserIdea, User

    winners = UserIdea.get_user_winners()
    place = 0
    for u, c in winners:
        if place > 2:
            return

        sticker_id = LEADERBOARD_STICKERS[place]
        user = User.get_user(u)

        text = "{}. {} ({})".format(place + 1, user.user_name, c)

        payload = {
            "text": text
        }
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer %s" % MIRO_TOKEN
        }
        requests.request("PATCH", MIRO_URL + "widgets/%s" % sticker_id, json=payload, headers=headers)
        place += 1
