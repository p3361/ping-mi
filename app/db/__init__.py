import datetime as dt
from uuid import uuid4
import logging
from sqlalchemy import create_engine, not_, and_
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
from sqlalchemy import (
    func, Integer,
    Column, BIGINT, DateTime, Boolean, Float, String,
    ForeignKey, Text
)
from sqlalchemy.dialects.postgresql import UUID, JSONB

from app.config import SQLALCHEMY_DATABASE_URL
from app.lib.miro import IdeaFields

engine = create_engine(SQLALCHEMY_DATABASE_URL)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session()

Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid4, index=True)
    email = Column(String(128))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    tg = relationship('TgUser', back_populates='user')
    slack = None
    ideas = relationship('UserIdea', back_populates='user')

    @staticmethod
    def add_user(email=None):
        try:
            user = User(email=email)
            session.add(user)
            session.commit()
            return user
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)

    @staticmethod
    def get_user(user_id):
        user = session.query(User).filter(User.id == user_id).first()
        return user

    @property
    def user_name(self):
        if self.tg:
            return self.tg[0].first_name or self.tg[0].username
        elif self.slack:
            return
        return "Mr. Neo"


class TgUser(Base):
    __tablename__ = "tg_users"

    id = Column(BIGINT, primary_key=True, index=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    first_name = Column(String(256))
    username = Column(String(256))
    is_bot = Column(Boolean, default=False)
    last_name = Column(String(256))
    language_code = Column(String(8))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    user = relationship('User', back_populates='tg')

    @staticmethod
    def add_user(chat_id, first_name, username, is_bot, last_name, language_code):
        try:
            tg_user = session.query(TgUser).filter(
                TgUser.id == chat_id
            ).first()
            if tg_user:
                return tg_user
            user = User.add_user()
            if not user:
                return
            tg_user = TgUser(
                id=chat_id,
                user_id=user.id,
                first_name=first_name,
                username=username,
                is_bot=is_bot,
                last_name=last_name,
                language_code=language_code
            )
            session.add(tg_user)
            session.commit()
            return tg_user
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)

    @staticmethod
    def get_user(chat_id):
        user = session.query(TgUser).filter(TgUser.id == chat_id).first()
        return user

    @staticmethod
    def get_all_users():
        users = session.query(TgUser).all()
        return users


class UserIdea(Base):
    __tablename__ = "user_ideas"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    idea = Column(Text)
    field = Column(String(128), index=True)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    sticker = relationship('MiroSticker', back_populates='idea')
    scores = relationship('IdeaScore', back_populates='idea')
    user = relationship('User', back_populates='ideas')

    @property
    def score(self):
        _score = IdeaScore.get_idea_score(self.id)
        return _score

    @staticmethod
    def add_idea(user_id, idea, field):
        try:
            idea = UserIdea(
                user_id=user_id,
                idea=idea,
                field=field
            )
            session.add(idea)
            session.commit()
            return idea
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)

    @staticmethod
    def get_idea(idea_id):
        idea = session.query(UserIdea).filter(UserIdea.id == idea_id).first()
        return idea

    @staticmethod
    def get_user_winners():
        stickers = session.query(UserIdea.user_id, func.COUNT(UserIdea.id).label("cnt")).filter(
            UserIdea.sticker != None
        ).group_by(UserIdea.user_id).all()
        return stickers


class MiroSticker(Base):
    __tablename__ = "miro_stickers"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    idea_id = Column(BIGINT, ForeignKey("user_ideas.id"), index=True)
    data = Column(JSONB)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    updated_at = Column(DateTime, default=dt.datetime.utcnow)

    idea = relationship('UserIdea', back_populates='sticker')

    @staticmethod
    def add_sticker(idea_id, data):
        try:
            sticker = MiroSticker(
                idea_id=idea_id, data=data
            )
            session.add(sticker)
            session.commit()
            return sticker
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)

    @staticmethod
    def num_of_stickers(field):
        cnt = session.query(MiroSticker).filter(
            MiroSticker.idea.has(UserIdea.field == field)
        ).count()
        return cnt

    @staticmethod
    def get_sticker(sticker_id):
        sticker = session.query(MiroSticker).filter(MiroSticker.id == sticker_id).first()
        return sticker

    @staticmethod
    def get_stickers(field):
        stickers = session.query(MiroSticker).filter(
            MiroSticker.idea.has(UserIdea.field == field)
        ).all()
        return stickers

    @staticmethod
    def update_sticker_data(sticker_id, data):
        try:
            sticker = session.query(MiroSticker).filter(
                MiroSticker.id == sticker_id
            ).first()
            if not sticker:
                return
            sticker.data = data
            sticker.updated_at = dt.datetime.utcnow()
            session.commit()
            return sticker
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)


class IdeaRateMessage(Base):
    __tablename__ = "idea_rate_messages"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    msg_id = Column(BIGINT)
    user_id = Column(UUID(as_uuid=True), index=True)
    idea_id = Column(BIGINT, index=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    @staticmethod
    def add_rate_message(msg_id, user_id, idea_id):
        try:
            m = IdeaRateMessage(
                msg_id=msg_id,
                user_id=user_id,
                idea_id=idea_id
            )
            session.add(m)
            session.commit()
            return m
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)

    @staticmethod
    def check_idea_message(user_id, idea_id):
        idea = UserIdea.get_idea(idea_id)
        if user_id == idea.user_id:
            return True
        c = session.query(IdeaRateMessage).filter(
            IdeaRateMessage.user_id == user_id,
            IdeaRateMessage.idea_id == idea_id
        ).first()
        return c is not None


class IdeaScore(Base):
    __tablename__ = "ideas_score"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    idea_id = Column(BIGINT, ForeignKey("user_ideas.id"), index=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey("users.id"), index=True)
    score = Column(Integer)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    updated_at = Column(DateTime, default=dt.datetime.utcnow)

    idea = relationship('UserIdea', back_populates='scores')

    @staticmethod
    def add_score(msg_id, user_id, score):
        try:
            m = session.query(IdeaRateMessage).filter(
                IdeaRateMessage.msg_id == msg_id
            ).first()
            if not m:
                return
            idea_id = m.idea_id
            _score = session.query(IdeaScore).filter(
                IdeaScore.idea_id == idea_id,
                IdeaScore.user_id == user_id
            ).first()
            if not _score:
                _score = IdeaScore(
                    idea_id=idea_id,
                    user_id=user_id,
                    score=score
                )
                session.add(_score)
                session.commit()
                return _score
            _score.score = score
            _score.updated_at = dt.datetime.utcnow()
            session.commit()
            return _score
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)

    @staticmethod
    def get_idea_score(idea_id):
        score = session.query(func.AVG(IdeaScore.score)).filter(
            IdeaScore.idea_id == idea_id
        ).scalar()
        return score or 0


class Logs(Base):
    __tablename__ = "logs"

    id = Column(BIGINT, primary_key=True, autoincrement=True)
    user_id = Column(UUID(as_uuid=True))
    message = Column(Text)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    @staticmethod
    def add_log(user_id, message):
        try:
            l = Logs(user_id=user_id, message=message)
            session.add(l)
            session.commit()
            return l
        except SQLAlchemyError as e:
            session.rollback()
            logging.error(e)
