import logging
from time import sleep
import telebot
from emoji import emojize

from app.config import TELEGRAM_BOT_TOKEN

from app.db import (
    TgUser, UserIdea, IdeaScore, Logs
)
from app.lib.miro import (
    IdeaFields,
    add_sticker, update_sticker_score, update_sort_stickers, update_winners_board
)
from app.lib.telegram.keyboards import (
    INFO_BTN, INFO_TEXT,
    IDEAS_BTN, BACK_BTN,
    DESIGN_BTN, DEVELOPMENT_BTN, MARKETING_BTN, SALES_BTN,
    default_keyboard, fields_keyboard
)
from app.lib.telegram.utils import send_ideas_for_rate


bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)


@bot.message_handler(commands=["start"])
def start(message):
    chat_id = message.chat.id
    first_name = message.from_user.first_name
    username = message.from_user.username
    is_bot = message.from_user.is_bot
    last_name = message.from_user.last_name
    language_code = message.from_user.language_code

    TgUser.add_user(chat_id, first_name, username, is_bot, last_name, language_code)

    keyboard = default_keyboard()
    text = INFO_TEXT
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


@bot.message_handler(regexp=INFO_BTN)
def info(message):
    keyboard = default_keyboard()
    text = INFO_TEXT
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


@bot.message_handler(commands=["idea"])
def idea(message):
    chat_id = message.chat.id
    msg = message.text
    idea = msg.replace("/idea", "").strip()
    user = TgUser.get_user(chat_id)
    if user and idea:
        UserIdea.add_idea(user.user_id, idea)

        keyboard = default_keyboard()
        text = "Thanks for your idea!"
        bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


@bot.message_handler(regexp=IDEAS_BTN)
def fields(message):
    keyboard = fields_keyboard()
    text = "Please choose field for your idea!"
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


@bot.message_handler(regexp=BACK_BTN)
def back(message):
    keyboard = default_keyboard()
    text = message.text
    bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)


def send_idea(message, field):
    keyboard = default_keyboard()
    chat_id = message.chat.id
    idea = message.text
    if idea in (BACK_BTN, DESIGN_BTN, DEVELOPMENT_BTN, MARKETING_BTN, SALES_BTN):
        text = idea
        bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)
        return
    user = TgUser.get_user(chat_id)
    if user and idea:
        # Add idea to db
        _idea = UserIdea.add_idea(user.user_id, idea, field)
        # Add idea to Miro
        add_sticker(_idea.id)
        text = "Thanks for your idea!"
        bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)
        # Send rate questions
        users = TgUser.get_all_users()
        for u in users:
            if u == user:
                continue
            send_ideas_for_rate(_idea, u.id)
            sleep(0.5)


def send_design_idea(message):
    send_idea(message, IdeaFields.design)


def send_development_idea(message):
    send_idea(message, IdeaFields.development)


def send_marketing_idea(message):
    send_idea(message, IdeaFields.marketing)


def send_sales_idea(message):
    send_idea(message, IdeaFields.sales)


def field(message, field: IdeaFields):
    keyboard = fields_keyboard()
    text = "Please send your {} idea!".format(emojize(":light_bulb:", use_aliases=True))
    sent = bot.send_message(message.chat.id, text, parse_mode="Markdown", reply_markup=keyboard)
    if field == IdeaFields.design:
        bot.register_next_step_handler(sent, send_design_idea)
    elif field == IdeaFields.development:
        bot.register_next_step_handler(sent, send_development_idea)
    elif field == IdeaFields.marketing:
        bot.register_next_step_handler(sent, send_marketing_idea)
    elif field == IdeaFields.sales:
        bot.register_next_step_handler(sent, send_sales_idea)


@bot.message_handler(regexp=DEVELOPMENT_BTN)
def development(message):
    field(message, IdeaFields.development)


@bot.message_handler(regexp=DESIGN_BTN)
def design(message):
    field(message, IdeaFields.design)


@bot.message_handler(regexp=MARKETING_BTN)
def marketing(message):
    field(message, IdeaFields.marketing)


@bot.message_handler(regexp=SALES_BTN)
def sales(message):
    field(message, IdeaFields.sales)


@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    try:
        chat_id = call.message.chat.id
        msg_id = call.message.message_id
        if "score" not in call.data:
            return
        score = int(call.data.split("score")[-1])
        user = TgUser.get_user(chat_id)
        s = IdeaScore.add_score(msg_id, user.user_id, score)
        text = "Thanks! 😊"
        keyboard = default_keyboard()
        bot.send_message(chat_id, text, parse_mode="Markdown", reply_markup=keyboard)
        update_sticker_score(s.idea_id)
        update_sort_stickers(s.idea_id)
        update_winners_board()
    except Exception as e:
        logging.error(e)


@bot.message_handler(func=lambda message: True)
def text(message):
    chat_id = message.chat.id
    _message = message.text
    user = TgUser.get_user(chat_id)
    Logs.add_log(user.user_id, _message)


if __name__ == "__main__":
    bot.infinity_polling()
